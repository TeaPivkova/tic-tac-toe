import React, { useState, useEffect } from 'react';
import { Route, Switch } from 'react-router-dom';
import NewGame from '../../components/Game';
import Home from '../Home';
import JoinGame from '../../components/JoinGame';


const Router = () => {
  return (
    <>
      <Switch>
        <Route exact path='/' component={Home} />
        <Route path='/game' component={NewGame} />
        <Route path='/joinGame' component={JoinGame} />
      </Switch>
    </>
  );
};
export default Router;

import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import './home.css';


const Home = () => {
    let clientId = null;
    let gameId = null;
    const ws = new WebSocket("ws://localhost:8080");
    
    useEffect(() => {
      ws.onmessage = message => {
        //messageData
        const response = JSON.parse(message.data);
        console.log(response);
        //connect
        if(response.method === "connect"){
           clientId = response.clientId;
          console.log("Client ID set succ");
        }
        //create
        if(response.method === "create"){
         gameId = response.game.id;
         console.log("HALLOOOOOOOOOOOOOOOOOOOOO");
       }
      
    }
     
    }, []);

    
    const startNewGame = () => {
      const payLoad = {
        "method": "create",
        "clientId": clientId
      }
      ws.send(JSON.stringify(payLoad));
    }
  
  return (
    <div className='center'>
      <div className='btn-group-vertical'>
        <Link className='btn btn-info btn-lg' onClick={startNewGame} to='/game'>
          New Game
        </Link>
        <Link className='btn btn-light btn-lg' to='/joinGame'>
          Join Game
        </Link>
      </div>
    </div>
  );
};

export default Home;
